package game;

import entities.characters.BasicEnemy;
import entities.characters.CharactersFactory;
import entities.characters.Mario;
import entities.collisions.BoundingBox;
import entities.objects.GameObject;
import entities.objects.GameObjectFactory;
import utils.Res;
import utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Platform extends JPanel {

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;
    private int collectedCoins = 0;

    private Mario mario;
    private Image imgFlag;
    private Image imgCastle;

    private List<GameObject> objects;
    private List<GameObject> pieces;
    private List<GameObject> powerUps;
    private List<BasicEnemy> enemies;

    private boolean debugDraw = false;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;

    public Platform() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);
        this.mario = new Mario(300, 245);
        this.initStage();
        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new Keyboard());
    }

    private void initStage() {
        final CharactersFactory charactersFactory = new CharactersFactory();
        enemies = new ArrayList<>();
        enemies.add(charactersFactory.createMushroom(800, 263));
        enemies.add(charactersFactory.createTurtle(950, 243));

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        final GameObjectFactory objectFactory = new GameObjectFactory();
        objects = new ArrayList<>();
        this.objects.add(objectFactory.createTunnel(600, 230));
        this.objects.add(objectFactory.createTunnel(1000, 230));
        this.objects.add(objectFactory.createTunnel(1600, 230));
        this.objects.add(objectFactory.createTunnel(1900, 230));
        this.objects.add(objectFactory.createTunnel(2500, 230));
        this.objects.add(objectFactory.createTunnel(3000, 230));
        this.objects.add(objectFactory.createTunnel(3800, 230));
        this.objects.add(objectFactory.createTunnel(4500, 230));
        this.objects.add(objectFactory.createBlock(400, 180));
        this.objects.add(objectFactory.createBlock(1200, 180));
        this.objects.add(objectFactory.createBlock(1270, 170));
        this.objects.add(objectFactory.createBlock(1340, 160));
        this.objects.add(objectFactory.createBlock(2000, 180));
        this.objects.add(objectFactory.createBlock(2600, 160));
        this.objects.add(objectFactory.createBlock(2650, 180));
        this.objects.add(objectFactory.createBlock(3500, 160));
        this.objects.add(objectFactory.createBlock(3550, 140));
        this.objects.add(objectFactory.createBlock(4000, 170));
        this.objects.add(objectFactory.createBlock(4200, 200));
        this.objects.add(objectFactory.createBlock(4300, 210));

        pieces = new ArrayList<>();
        this.pieces.add(objectFactory.createPiece(1202, 140));
        this.pieces.add(objectFactory.createPiece(1272, 95));
        this.pieces.add(objectFactory.createPiece(1342, 40));
        this.pieces.add(objectFactory.createPiece(1650, 145));
        this.pieces.add(objectFactory.createPiece(2650, 145));
        this.pieces.add(objectFactory.createPiece(3000, 135));
        this.pieces.add(objectFactory.createPiece(3400, 125));
        this.pieces.add(objectFactory.createPiece(4200, 145));
        this.pieces.add(objectFactory.createPiece(4600, 40));

        powerUps = new ArrayList<>();
        this.powerUps.add(objectFactory.creteMushroomPowerup(402, 158));
    }

    public void setDebugDraw(boolean flag) {
        this.debugDraw = flag;
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getXPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    public Mario getMario() {
        return this.mario;
    }

    private void drawGameObjects(Graphics g2, List<GameObject> objectsToDraw) {
        objectsToDraw.forEach(o -> {
            g2.drawImage(o.getImgObj(), o.getX(), o.getY(), null);
            if (debugDraw) {
                final BoundingBox bbox = o.getBoundingBox();
                g2.drawRect(bbox.getMinX(), bbox.getMinY(), o.getWidth(), o.getHeight());
            }
        });
    }

    private void drawEnemies(Graphics g2) {
        enemies.forEach(e -> {
            if (e.isAlive()) {
                g2.drawImage(e.getWalkingImage(), e.getX(), e.getY(), null);
            } else {
                g2.drawImage(e.getDeadImage(), e.getX(), e.getY() + e.getDeadOffset(), null);
            }
        });
    }

    private void moveFixedObjects(Graphics g2) {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            objects.forEach(GameObject::move);
            pieces.forEach(GameObject::move);
            enemies.forEach(BasicEnemy::step);
            powerUps.forEach(GameObject::move);
        }
    }

    private void drawBackground(Graphics g2) {
        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);
    }

    private void solveCollisions() {

        boolean hitSomething = false;

        for (final GameObject o: objects) {
            if (mario.isNearby(o)) {
                hitSomething = true;
                this.mario.contactWithObject(o);
            }

            enemies.forEach(e-> {
                objects.forEach(o2 -> e.contactWithObject(o2));
            });
        }

        if (!hitSomething) {
            mario.reset();
        }

        final List<GameObject> toRemove = new ArrayList<>();
        pieces.forEach(p -> {
            if (this.mario.isNearby(p)) {
                Audio.playSound(Res.AUDIO_MONEY);
                toRemove.add(p);
                collectedCoins++;
            }
        });
        pieces.removeAll(toRemove);

        enemies.forEach(e -> {
            enemies.forEach(e2 -> {
                if (e.isNearby(e2)) {
                    e.contactWithCharacter(e2);
                }
            });
            if (mario.isNearby(e)) {
                mario.contactWithCharacter(e);
            }
        });
    }

    private void drawCastleAndFlag(Graphics g2) {
        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);
    }

    private void drawMario(Graphics g2) {
        if (this.mario.isJumping()) {
            g2.drawImage(this.mario.getJumpImage(), this.mario.getX(), this.mario.getY(), null);
        } else {
            g2.drawImage(this.mario.getWalkingImage(), this.mario.getX(), this.mario.getY(), null);
        }
    }

    private void debugDraw(Graphics g2) {
        final BoundingBox bbox = mario.getBoundingBox();
        g2.drawRect(bbox.getMinX(), bbox.getMinY(), mario.getWidth(), mario.getHeight());

        g2.fillOval(bbox.getMaxX() + 10, bbox.getCenterY(), 3, 3);
        g2.fillOval(bbox.getMinX() - 10, bbox.getCenterY(), 3, 3);
        g2.fillOval(bbox.getCenterX(), bbox.getMaxY() + 10, 3, 3);
        g2.fillOval(bbox.getCenterX(), bbox.getMinY() - 10, 3, 3);

        enemies.forEach(e -> {
            final BoundingBox enemyBoundingBox = e.getBoundingBox();
            g2.drawRect(enemyBoundingBox.getMinX(), enemyBoundingBox.getMinY(), e.getWidth(), e.getHeight());
        });
    }

    private void drawCollectedCoins(Graphics g2) {
        g2.drawImage(Utils.getImage(Res.IMG_PIECE1), 10, 10, null);
        g2.drawString(Integer.toString(collectedCoins), 40, 30);
    }

    public void paintComponent(Graphics g2) {
        super.paintComponent(g2);
        this.solveCollisions();
        this.updateBackgroundOnMovement();
        this.moveFixedObjects(g2);
        this.drawBackground(g2);
        this.drawGameObjects(g2, objects);
        this.drawGameObjects(g2, pieces);
        this.drawGameObjects(g2, powerUps);
        this.drawCastleAndFlag(g2);
        this.drawMario(g2);
        this.drawEnemies(g2);
        this.drawCollectedCoins(g2);

        if (debugDraw) {
            this.debugDraw(g2);
        }

    }
}
