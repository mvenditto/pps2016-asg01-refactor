package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.getScene().getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Main.getScene().getXPos() == -1) {
                    Main.getScene().setXPos(0);
                    Main.getScene().setBackground1PosX(-50);
                    Main.getScene().setBackground2PosX(750);
                }
                Main.getScene().getMario().setMoving(true);
                Main.getScene().getMario().setFacingRight(true);
                Main.getScene().setMov(1); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.getScene().getXPos() == 4601) {
                    Main.getScene().setXPos(4600);
                    Main.getScene().setBackground1PosX(-50);
                    Main.getScene().setBackground2PosX(750);
                }

                Main.getScene().getMario().setMoving(true);
                Main.getScene().getMario().setFacingRight(false);
                Main.getScene().setMov(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.getScene().getMario().setJumping(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.getScene().getMario().setMoving(false);
        Main.getScene().setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
