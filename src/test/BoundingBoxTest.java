package test;

import entities.collisions.BoundingBox;
import entities.collisions.ContactDetectionStrategy;
import entities.collisions.ContactDirection;
import entities.collisions.FourDirectionContactDetection;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class BoundingBoxTest {

    private BoundingBox boundingBox;
    private ContactDetectionStrategy strategy;

    private final static int MIN_X = 0;
    private final static int WIDTH = 20;
    private final static int MIN_Y = 0;
    private final static int HEIGHT = 10;
    private final static int PROXIMITY_RANGE = 10;
    private final static int HALF_PROXIMITY_RANGE = PROXIMITY_RANGE / 2;

    @Before
    public void setUp() throws Exception {
        this.strategy = new FourDirectionContactDetection(10);
        this.boundingBox = new BoundingBox(strategy, MIN_X, MIN_Y, WIDTH, HEIGHT);
    }

    @Test
    public void testShouldNotIntersect() throws Exception {
        BoundingBox bbox = new BoundingBox(strategy, MIN_X + WIDTH * 2, MIN_Y + HEIGHT * 2, WIDTH, HEIGHT);
        assertFalse(boundingBox.intersects(bbox));
    }

    @Test
    public void testShouldIntersect() throws Exception {
        BoundingBox bbox = new BoundingBox(strategy, MIN_X + WIDTH / 2, MIN_Y + HEIGHT / 2, WIDTH, HEIGHT);
        assertTrue(boundingBox.intersects(bbox));
    }

    @Test
    public void testShouldContactAhead() throws Exception {
        BoundingBox aheadBox = new BoundingBox(strategy, MIN_X + WIDTH + HALF_PROXIMITY_RANGE, MIN_Y, WIDTH, HEIGHT);
        assertTrue(boundingBox.findContacts(aheadBox).contains(ContactDirection.AHEAD));
    }

    @Test
    public void testShouldContactBack() throws Exception {
        BoundingBox backBox = new BoundingBox(strategy,MIN_X - WIDTH - HALF_PROXIMITY_RANGE, MIN_Y, WIDTH, HEIGHT);
        assertTrue(boundingBox.findContacts(backBox).contains(ContactDirection.BACK));
    }

    @Test
    public void testShouldContactAbove() throws Exception {
        BoundingBox aboveBox = new BoundingBox(strategy, MIN_X, MIN_Y - HEIGHT- HALF_PROXIMITY_RANGE, WIDTH, HEIGHT);
        assertTrue(boundingBox.findContacts(aboveBox).contains(ContactDirection.ABOVE));
    }

    @Test
    public void testShouldContactBelow() throws Exception {
        BoundingBox bbox = new BoundingBox(strategy, MIN_X, MIN_Y + HEIGHT + HALF_PROXIMITY_RANGE, WIDTH, HEIGHT);
        assertTrue(boundingBox.findContacts(bbox).contains(ContactDirection.BELOW));
    }

    @Test
    public void testShouldContainPoint() throws Exception {
        assertTrue(boundingBox.contains(boundingBox.getCenterX(), boundingBox.getCenterY()));
    }

}