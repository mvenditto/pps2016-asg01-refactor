package entities;

import entities.collisions.BoundingBox;
import entities.collisions.ContactDetectionStrategy;
import entities.collisions.FourDirectionContactDetection;

public abstract class BasicGameEntity implements GameEntity {

    protected int x;
    protected int y;
    protected int width;
    protected int height;
    private BoundingBox boundingBox;

    private final static int PROXIMITY_MARGIN = 10;

    protected BasicGameEntity(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.width = w;
        this.height = h;

        final ContactDetectionStrategy strategy = new FourDirectionContactDetection(PROXIMITY_MARGIN);
        this.boundingBox = new BoundingBox(strategy, x, y, w, h);
    }

    @Override
    public void setX(int x) {
        this.x = x;
        this.boundingBox.setMinX(x);
    }

    @Override
    public void setY(int y) {
        this.y = y;
        this.boundingBox.setMinY(y);
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public BoundingBox getBoundingBox() {
        return this.boundingBox;
    }

}
