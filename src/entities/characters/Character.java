package entities.characters;

import entities.GameEntity;
import entities.collisions.ContactDirection;

import java.awt.*;

public interface Character {

    Image getWalkingImage();

    void step();

    boolean isAlive();

    boolean isFacingRight();

    void setAlive(boolean alive);

    void setMoving(boolean moving);

    void setFacingRight(boolean toRight);

    boolean isNearby(GameEntity e);

    boolean hitAtDirection(GameEntity e, ContactDirection dir);

    void contactWithObject(GameEntity obj);

    void contactWithCharacter(BasicCharacter pers);

}