package entities.characters.mario_behaviors;

/**
 * Created by mvenditto.
 */
public class MarioFireBehavior extends DefaultMarioBehavior {

    private static final String marioBehaviorName = "mario_fire";

    @Override
    public String getBehaviorName() {
        return marioBehaviorName;
    }

}
