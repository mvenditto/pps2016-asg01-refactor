package entities.characters.mario_behaviors;

public interface MarioBehavior {

    int getJumpHeightLimit();

    int getMarioFrequency();

    String getBehaviorName();

}
