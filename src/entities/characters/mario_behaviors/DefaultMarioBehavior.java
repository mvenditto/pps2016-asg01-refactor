package entities.characters.mario_behaviors;


public class DefaultMarioBehavior implements MarioBehavior{

    private static final String marioBehaviorName = "mario";
    private static final int jumpHeightLimit = 42;
    private static final int marioFrequency = 25;

    @Override
    public int getJumpHeightLimit() {
        return jumpHeightLimit;
    }

    @Override
    public int getMarioFrequency() {
        return marioFrequency;
    }

    @Override
    public String getBehaviorName() {
        return marioBehaviorName;
    }
}
