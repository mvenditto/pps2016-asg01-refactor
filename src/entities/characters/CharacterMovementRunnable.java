package entities.characters;

public class CharacterMovementRunnable implements Runnable {

    private Character character;
    private final static int PAUSE = 15;

    public CharacterMovementRunnable(BasicCharacter character) {
        this.character = character;
    }

    @Override
    public void run() {
        while (true) {
            if (character.isAlive()) {
                character.step();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    System.err.println(e.getMessage());
                }
            }
        }
    }
}
