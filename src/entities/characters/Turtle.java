package entities.characters;

import utils.Res;
import utils.Utils;

import java.awt.*;

public class Turtle extends BasicEnemy {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT, TURTLE_DEAD_OFFSET_Y);
        super.setFacingRight(true);
        super.setMoving(true);
    }

    @Override
    public Image getDeadImage()  {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }

    @Override
    public Image getWalkingImage() {
        return Utils.getImage(this.getWalkingImageName(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY));
    }
}
