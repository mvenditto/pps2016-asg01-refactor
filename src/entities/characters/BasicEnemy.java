package entities.characters;

import entities.GameEntity;

import java.awt.*;

import static entities.collisions.ContactDirection.AHEAD;
import static entities.collisions.ContactDirection.BACK;

public abstract class BasicEnemy extends BasicCharacter {

    private int moveDirection = 1;
    private int deadOffset;

    BasicEnemy(int x, int y, int width, int height, int deadOffset) {
        super(x, y, width, height);
        this.setFacingRight(true);
        this.setMoving(true);
        this.deadOffset = deadOffset;
        final Thread updateThread = new Thread(new CharacterMovementRunnable(this));
        updateThread.start();
    }

    public int getDeadOffset() {
        return this.deadOffset;
    }

    @Override
    public void step() {
        this.moveDirection = isFacingRight() ? 1 : -1;
        super.setX(super.getX() + this.moveDirection);
    }

    @Override
    public void contactWithObject(GameEntity e) {
        if (this.hitAtDirection(e, AHEAD) && this.isFacingRight()) {
            this.setFacingRight(false);
            this.moveDirection = -1;
        } else if (this.hitAtDirection(e, BACK) && !this.isFacingRight()) {
            this.setFacingRight(true);
            this.moveDirection = 1;
        }
    }

    @Override
    public void contactWithCharacter(BasicCharacter character) {
        this.contactWithObject(character);
    }

    abstract public Image getDeadImage();
}
