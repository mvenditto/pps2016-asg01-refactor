package entities.characters;

import entities.GameEntity;
import entities.characters.mario_behaviors.DefaultMarioBehavior;
import entities.characters.mario_behaviors.MarioBehavior;
import game.Main;
import utils.Res;
import utils.Utils;

import java.awt.*;

import static entities.collisions.ContactDirection.*;

public class Mario extends BasicCharacter implements PlayableCharacter {

    private boolean jumping;
    private int jumpingExtent;
    private MarioBehavior marioBehavior;

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;

    public Mario(MarioBehavior behavior, int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.jumping = false;
        this.jumpingExtent = 0;
        this.marioBehavior = behavior;
    }

    public Mario(int x, int y) {
        this(new DefaultMarioBehavior(), x, y);
    }

    @Override
    public Image getJumpImage() {
        String status;
        final String facing = this.isFacingRight() ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX;

        this.jumpingExtent++;

        if (this.jumpingExtent < marioBehavior.getJumpHeightLimit()) {
            if (this.getY() > Main.getScene().getHeightLimit()) {
                this.setY(this.getY() - 4);
            } else {
                this.jumpingExtent = marioBehavior.getJumpHeightLimit();
            }
            status = Res.IMGP_STATUS_SUPER;
        } else if (this.getY() + this.getHeight() < Main.getScene().getFloorOffsetY()) {
            this.setY(this.getY() + 1);
            status = Res.IMGP_STATUS_SUPER;
        } else {
            status = Res.IMGP_STATUS_ACTIVE;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(Res.IMG_BASE + marioBehavior.getBehaviorName() + status + facing + Res.IMG_EXT);
    }

    @Override
    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    @Override
    public boolean isJumping() {
        return jumping;
    }

    @Override
    public void contactWithObject(GameEntity obj) {
        final boolean hitAhead = this.hitAtDirection(obj, AHEAD);
        final boolean hitBack = this.hitAtDirection(obj, BACK);
        final boolean hitAbove = this.hitAtDirection(obj, ABOVE);
        final boolean hitBelow = this.hitAtDirection(obj, BELOW);

        if (hitAhead && this.isFacingRight() || hitBack && !this.isFacingRight()) {
            Main.getScene().setMov(0);
            this.setMoving(false);
        }

        if (hitBelow) {
            Main.getScene().setFloorOffsetY(obj.getY());
        } else {
            Main.getScene().setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove) {
                Main.getScene().setHeightLimit(obj.getY() + obj.getHeight());
            } else if (!hitAbove && !this.jumping) {
                Main.getScene().setHeightLimit(0);
            }
        }
    }

    @Override
    public void contactWithCharacter(BasicCharacter chr) {
        if (this.hitAtDirection(chr, AHEAD) || this.hitAtDirection(chr, BACK)) {
            if (chr.isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
            } else {
                this.setAlive(true);
            }
        } else if (this.hitAtDirection(chr, BELOW)) {
            chr.setMoving(false);
            chr.setAlive(false);
        }
    }

    @Override
    public void step() {
        if (Main.getScene().getXPos() >= 0) {
            this.x = this.x - Main.getScene().getMov();
        }
    }

    @Override
    public Image getWalkingImage() {
        return Utils.getImage(this.getWalkingImageName(marioBehavior.getBehaviorName(), marioBehavior.getMarioFrequency()));
    }

    public void setMarioBehavior(MarioBehavior behavior) {
        this.marioBehavior = behavior;
    }

    public void reset() {
        Main.getScene().setHeightLimit(0);
        Main.getScene().setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
    }
}
