package entities.objects;


public class GameObjectFactory implements AbstractGameObjectFactory {

    @Override
    public GameObject createTunnel(int x, int y) {
        return new Tunnel(x, y);
    }

    @Override
    public GameObject createBlock(int x, int y) {
        return new Block(x, y);
    }

    @Override
    public GameObject createPiece(int x, int y) {
        return new Piece(x, y);
    }

    @Override
    public GameObject creteMushroomPowerup(int x, int y) {
        return new MushroomPowerUp(x, y);
    }
}
