package entities.objects;

import java.awt.Image;

import entities.BasicGameEntity;
import game.Main;

public class GameObject extends BasicGameEntity {

    protected Image imgObj;

    public GameObject(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public Image getImgObj() {
        return imgObj;
    }

    public void move() {
        if (Main.getScene().getXPos() >= 0) {
            this.setX(this.getX() - Main.getScene().getMov());
        }
    }
}
