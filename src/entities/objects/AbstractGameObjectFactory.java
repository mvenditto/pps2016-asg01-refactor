package entities.objects;

public interface AbstractGameObjectFactory {

    GameObject createTunnel(int x, int y);

    GameObject createBlock(int x, int y);

    GameObject createPiece(int x, int y);

    GameObject creteMushroomPowerup(int x, int y);

}
