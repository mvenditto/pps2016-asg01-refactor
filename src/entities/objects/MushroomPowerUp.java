package entities.objects;

import utils.Res;
import utils.Utils;

/**
 * Created by mvenditto.
 */
public class MushroomPowerUp extends GameObject {

    private static final int WIDTH = 26;
    private static final int HEIGHT = 25;

    public MushroomPowerUp(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imgObj = Utils.getImage(Res.IMG_BIG_MARIO_POWER_UP);
    }

}
