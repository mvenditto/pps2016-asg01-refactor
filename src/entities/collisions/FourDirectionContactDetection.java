package entities.collisions;

import java.util.EnumSet;


public class FourDirectionContactDetection implements ContactDetectionStrategy {

    private int contactRadius = 10;

    public FourDirectionContactDetection(int contactRadius) {
        this.contactRadius = contactRadius;
    }

    public FourDirectionContactDetection() {
        this(0);
    }

    @Override
    public EnumSet<ContactDirection> findContactDirections(BoundingBox a, BoundingBox b) {
        final EnumSet<ContactDirection> contactPosition = EnumSet.noneOf(ContactDirection.class);

        final int max_x = a.getMaxX();
        final int max_y = a.getMaxY();
        final int center_x = a.getCenterX();
        final int center_y = a.getCenterY();

        if (b.contains(max_x + contactRadius, center_y)) {
            contactPosition.add(ContactDirection.AHEAD);
        }
        if (b.contains(a.getMinX() - contactRadius, center_y)) {
            contactPosition.add(ContactDirection.BACK);
        }

        if (b.contains(center_x, a.getMinY() - contactRadius)) {
            contactPosition.add(ContactDirection.ABOVE);
        }
        if (b.contains(center_x, max_y + contactRadius)) {
            contactPosition.add(ContactDirection.BELOW);
        }
        return contactPosition;
    }

}
