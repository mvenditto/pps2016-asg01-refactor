package entities.collisions;

import java.util.EnumSet;

public interface ContactDetectionStrategy {

    EnumSet<ContactDirection> findContactDirections(BoundingBox a, BoundingBox b);

}
