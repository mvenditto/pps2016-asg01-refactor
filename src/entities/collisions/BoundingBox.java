package entities.collisions;

import java.util.EnumSet;


public class BoundingBox {

    private int min_x;
    private int min_y;
    private int width;
    private int height;
    private ContactDetectionStrategy strategy;

    private final static String LEFT_BRACE = "(";
    private final static String RIGHT_BRACE = ")";

    public BoundingBox(ContactDetectionStrategy strategy,
                       int x, int y, int width, int height) {
        this.min_x = x;
        this.min_y = y;
        this.strategy = strategy;
        this.resize(width, height);
    }

    public boolean intersects(BoundingBox other) {
        return this.intersects(other, 0);
    }

    public boolean intersects(BoundingBox other, int collRadius) {
        return min_x - collRadius < other.getMaxX() &&
                this.getMaxX() + collRadius > other.getMinX() &&
                min_y - collRadius < other.getMaxY() &&
                this.getMaxY() + collRadius > other.getMinY();
    }

    public boolean contains(int x, int y) {
        boolean contains_x = (x >= min_x && x <= getMaxX());
        boolean contains_y = (y >= min_y && y <= getMaxY());
        return contains_x && contains_y;
    }

    public EnumSet<ContactDirection> findContacts(BoundingBox other) {
        return this.strategy.findContactDirections(this, other);
    }

    public int getCenterX() {
        return this.min_x + (this.width / 2);
    }

    public int getCenterY() {
        return this.min_y + (this.height / 2);
    }

    public int getMinX() {
        return this.min_x;
    }

    public int getMinY() {
        return this.min_y;
    }

    public int getMaxX() {
        return this.min_x + this.width;
    }

    public int getMaxY() {
        return  this.min_y + this.height;
    }

    public void setMinX(int new_min_x) {
        this.min_x = new_min_x;
    }

    public void setMinY(int new_min_y) {
        this.min_y = new_min_y;
    }

    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return new StringBuilder(LEFT_BRACE)
                .append(min_x)
                .append(min_y)
                .append(min_x + width)
                .append(min_y + height)
                .append(RIGHT_BRACE)
                .toString();
    }
}
