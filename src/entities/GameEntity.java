package entities;

import entities.collisions.BoundingBox;

public interface GameEntity {

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    void setX(int x);

    void setY(int y);

    void setWidth(int w);

    void setHeight(int h);

    BoundingBox getBoundingBox();

}
